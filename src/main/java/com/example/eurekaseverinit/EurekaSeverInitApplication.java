package com.example.eurekaseverinit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaSeverInitApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaSeverInitApplication.class, args);
    }

}
